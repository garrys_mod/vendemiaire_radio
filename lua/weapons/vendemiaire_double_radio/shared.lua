SWEP.PrintName = "Double radio"

SWEP.Author = "(Pyth0n11)"
SWEP.Purpose = "attack1 = config the radio."

SWEP.Spawnable = true
SWEP.DisableDuplicator = true
SWEP.ViewModel = ""
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ViewModelFOV = 50
SWEP.Category = "Vendémiaire's radio"
-- Slot start at 0 so 5 = slot 6.
SWEP.Slot = 5

SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1

SWEP.Secondary.Automatic = false

SWEP.DrawCrosshair = false
SWEP.DrawAmmo = false

SWEP.last_receiving_reset = 0

SWEP.radio1_isactive = false
SWEP.radio1_frequency = 1
SWEP.radio1_speak_button = 12 -- 12 = KEY_B
SWEP.radio1_speak_button_status = false
SWEP.radio1_is_speaking = false -- Do the player is speaking on radio.
SWEP.radio1_is_receiving = false -- Do the radio is receiving voice.

SWEP.radio2_isactive = false
SWEP.radio2_frequency = 2
SWEP.radio2_speak_button = 18 -- 18 = KEY_H
SWEP.radio2_speak_button_status = false
SWEP.radio2_is_speaking = false -- Do the player is speaking on radio.
SWEP.radio2_is_receiving = false -- Do the radio is receiving voice.

local change_settings = "radio/change_settings.wav"

-- Set the hold type
function SWEP:Initialize()
   self:SetHoldType("normal")
end

-- Get the client config and send it to the server.
if SERVER then
   function SWEP:Equip(new_owner)
      timer.Simple(1, function()
         net.Start("load_vendemiaire_double_radio_client_settings")
         net.Send(new_owner)
      end)
   end
end

-------------------
-- Settings menu --
-------------------

-- Open a config menu for the radio.
function SWEP:PrimaryAttack()
    if (game.SinglePlayer()) then self:CallOnClient("PrimaryAttack") end -- Force the client to read PrimaryAttack function in singleplayer.
    if not IsFirstTimePredicted() then return end -- If it not checking first time predicted, it will be done many times.
    
    self:SetNextPrimaryFire(CurTime()+1)
    
    if not CLIENT then return end

    local frame = vgui.Create("DFrame")
    frame:SetPos(ScrW()/2 - 350, ScrH()/2 - 150)
    frame:SetSize(700, 300)
    frame:SetTitle("Radio settings")
    frame:MakePopup()

    local label_r1 = vgui.Create("DLabel", frame)
    label_r1:SetPos(100, 30)
    label_r1:SetText("Radio 1")
    label_r1:SizeToContents()
        
    local numslider_r1 = vgui.Create("DNumSlider", frame)
    numslider_r1:SetSize(300, 100)
    numslider_r1:SetPos(15, 30)
    numslider_r1:SetText("frequency")
    numslider_r1:SetMin(1)
    numslider_r1:SetMax(1000)
    numslider_r1:SetDecimals(0)
    numslider_r1:SetValue(self.radio1_frequency)
        
    local binderlabel_r1 = vgui.Create("DLabel", frame)
    binderlabel_r1:SetPos(90, 150)
    binderlabel_r1:SetText("Speak button :")
    binderlabel_r1:SizeToContents()
    
    local binder_r1 = vgui.Create("DBinder", frame)
    binder_r1:SetPos(80, 170)
    binder_r1:SetSize(100, 50)
    binder_r1:SetValue(self.radio1_speak_button)

    local checkbox_r1 = vgui.Create("DCheckBoxLabel", frame)
    checkbox_r1:SetPos(100, 120)
    checkbox_r1:SetText("Turn on")
    checkbox_r1:SetValue(self.radio1_isactive)
    
    local label_r2 = vgui.Create("DLabel", frame)
    label_r2:SetPos(550, 30)
    label_r2:SetText("Radio 2")
    label_r2:SizeToContents()
        
    local numslider_r2 = vgui.Create("DNumSlider", frame)
    numslider_r2:SetSize(300, 100)
    numslider_r2:SetPos(410, 30)
    numslider_r2:SetText("frequency")
    numslider_r2:SetMin(1)
    numslider_r2:SetMax(1000)
    numslider_r2:SetDecimals(0)
    numslider_r2:SetValue(self.radio2_frequency)
        
    local binderlabel_r2 = vgui.Create("DLabel", frame)
    binderlabel_r2:SetPos(530, 150)
    binderlabel_r2:SetText("Speak button :")
    binderlabel_r2:SizeToContents()
    
    local binder_r2 = vgui.Create("DBinder", frame)
    binder_r2:SetPos(520, 170)
    binder_r2:SetSize(100, 50)
    binder_r2:SetValue(self.radio2_speak_button)

    local checkbox_r2 = vgui.Create("DCheckBoxLabel", frame)
    checkbox_r2:SetPos(540, 120)
    checkbox_r2:SetText("Turn on")
    checkbox_r2:SetValue(self.radio2_isactive)
    
    local DermaButton = vgui.Create("DButton", frame)
    DermaButton:SetText("Apply")
    DermaButton:SetPos(250, 240)
    DermaButton:SetSize(200, 30)
    DermaButton.DoClick = function()
       -- Change the client's config.
       -- Save the radio1's frequency if it change.
       if self.radio1_frequency != math.floor(numslider_r1:GetValue()) then
	  self.radio1_frequency = math.floor(numslider_r1:GetValue())
	   GetConVar("double_radio1_frequency"):SetInt(math.floor(numslider_r1:GetValue()))
       end
       -- Save radio1's speak_button if it change.
       if binder_r1:GetValue() != self.radio1_speak_button then
	  self.radio1_speak_button = binder_r1:GetValue()
	  GetConVar("double_radio1_speak_button"):SetInt(binder_r1:GetValue())
       end
       -- Save the radio1's isactive state if it change
       if self.radio1_isactive != checkbox_r1:GetChecked() then
	  self.radio1_isactive = checkbox_r1:GetChecked()
	  GetConVar("double_radio1_isactive"):SetBool(checkbox_r1:GetChecked())
       end
       
       -- Save the radio2's frequency if it change.
       if self.radio2_frequency != math.floor(numslider_r2:GetValue()) then
	  self.radio2_frequency = math.floor(numslider_r2:GetValue())
	   GetConVar("double_radio2_frequency"):SetInt(math.floor(numslider_r2:GetValue()))
       end
       -- Save radio2's speak_button if it change.
       if binder_r2:GetValue() != self.radio2_speak_button then
	  self.radio2_speak_button = binder_r2:GetValue()
	  GetConVar("double_radio2_speak_button"):SetInt(binder_r2:GetValue())
       end
       -- Save the radio2's isactive state if it change
       if self.radio2_isactive != checkbox_r2:GetChecked() then
	  self.radio2_isactive = checkbox_r2:GetChecked()
	  GetConVar("double_radio2_isactive"):SetBool(checkbox_r2:GetChecked())
       end
       
       -- Send the config update to the server.
       net.Start("update_vendemiaire_double_radio_settings")
           net.WriteInt(self.radio1_frequency, 32)
	   net.WriteInt(self.radio1_speak_button, 32)
	   net.WriteBool(self.radio1_isactive)
	   net.WriteInt(self.radio2_frequency, 32)
	   net.WriteInt(self.radio2_speak_button, 32)
	   net.WriteBool(self.radio2_isactive)
       net.SendToServer()
       self:EmitSound(change_settings)
   end
end

function SWEP:SecondaryAttack()
end
