SWEP.PrintName = "Simple radio"

SWEP.Author = "(Pyth0n11)"
SWEP.Purpose = "attack1 = config the radio."

SWEP.Spawnable = true
SWEP.DisableDuplicator = true
SWEP.ViewModel = ""
SWEP.WorldModel = "models/weapons/w_pistol.mdl"
SWEP.ViewModelFOV = 50
SWEP.Category = "Vendémiaire's radio"
-- Slot start at 0 so 5 = slot 6.
SWEP.Slot = 5

SWEP.Primary.Automatic = false
SWEP.Primary.Ammo = "none"
SWEP.Primary.ClipSize = -1
SWEP.Primary.DefaultClip = -1

SWEP.Secondary.Automatic = false

SWEP.DrawCrosshair = false
SWEP.DrawAmmo = false

SWEP.last_receiving_reset = 0

SWEP.isactive = true
SWEP.frequency = 1
SWEP.speak_button = 24 -- 24 = KEY_N
SWEP.speak_button_status = false
SWEP.is_speaking = false -- Do the player is speaking on radio.
SWEP.is_receiving = false -- Do the radio is receiving voice.

local change_settings = "radio/change_settings.wav"

-- Set the hold type
function SWEP:Initialize()
    self:SetHoldType("normal")
end

-- Get the client config and send it to the server.
if SERVER then
   function SWEP:Equip(new_owner)
      timer.Simple(1, function()
         net.Start("load_vendemiaire_simple_radio_client_settings")
         net.Send(new_owner)
      end)
   end
end

-------------------
-- Settings menu --
-------------------

-- Open a config menu for the radio.
function SWEP:PrimaryAttack()
    if (game.SinglePlayer()) then self:CallOnClient("PrimaryAttack") end -- Force the client to read PrimaryAttack function in singleplayer.
    if not IsFirstTimePredicted() then return end -- If it not checking first time predicted, it will be done many times.
    
    self:SetNextPrimaryFire(CurTime()+1)
    
    if not CLIENT then return end

    local frame = vgui.Create("DFrame")
    frame:SetPos(ScrW()/2 - 250, ScrH()/2 - 150)
    frame:SetSize(500, 300)
    frame:SetTitle("Radio settings")
    frame:MakePopup()
        
    local numslider = vgui.Create("DNumSlider", frame)
    numslider:SetSize(300, 100)
    numslider:SetPos(100, 30)
    numslider:SetText("frequency")
    numslider:SetMin(1)
    numslider:SetMax(1000)
    numslider:SetDecimals(0)
    numslider:SetValue(self.frequency)
        
    local binderlabel = vgui.Create("DLabel", frame)
    binderlabel:SetPos(200, 150)
    binderlabel:SetText("Speak button :")
    binderlabel:SizeToContents()
    
    local binder = vgui.Create("DBinder", frame)
    binder:SetPos(200, 170)
    binder:SetSize(100, 50)
    binder:SetValue(self.speak_button)

    local checkbox = vgui.Create("DCheckBoxLabel", frame)
    checkbox:SetPos(200, 120)
    checkbox:SetText("Turn on")
    checkbox:SetValue(self.isactive)
        
    local DermaButton = vgui.Create("DButton", frame)
    DermaButton:SetText("Apply")
    DermaButton:SetPos(150, 240)
    DermaButton:SetSize(200, 30)
    DermaButton.DoClick = function()
       -- Change the client's config.
       -- Save the frequency if it change.
       if self.frequency != math.floor(numslider:GetValue()) then
	  self.frequency = math.floor(numslider:GetValue())
	   GetConVar("simple_radio_frequency"):SetInt(math.floor(numslider:GetValue()))
       end
       -- Save speak_button if it change.
       if binder:GetValue() != self.speak_button then
	  self.speak_button = binder:GetValue()
	  GetConVar("simple_radio_speak_button"):SetInt(binder:GetValue())
       end

       -- Save the isactive state if it change
       if self.isactive != checkbox:GetChecked() then
	  self.isactive = checkbox:GetChecked()
	  GetConVar("simple_radio_isactive"):SetBool(checkbox:GetChecked())
       end
    	    
       -- Send the config update to the server.
       net.Start("update_vendemiaire_simple_radio_settings")
           net.WriteInt(self.frequency, 32)
	   net.WriteInt(self.speak_button, 32)
	   net.WriteBool(self.isactive)
       net.SendToServer()
       self:EmitSound(change_settings)
   end
end

function SWEP:SecondaryAttack()
end
