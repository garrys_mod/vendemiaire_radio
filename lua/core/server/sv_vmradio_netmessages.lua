util.AddNetworkString("switch_vmradio_frequency_radio_show_name")
util.AddNetworkString("update_vendemiaire_simple_radio_settings")
util.AddNetworkString("update_vendemiaire_double_radio_settings")
util.AddNetworkString("load_vendemiaire_simple_radio_client_settings")
util.AddNetworkString("load_vendemiaire_double_radio_client_settings")


-- Change the show name convar when admin request it.
net.Receive("switch_vmradio_frequency_radio_show_name", function(len, ply)
    if not ply:IsSuperAdmin() then
        ply:PrintMessage(3, "You're are not allow to do that.")
        return
    end
    GetConVar("vmradio_frequency_radio_show_name"):SetBool(!(GetConVar("vmradio_frequency_radio_show_name"):GetBool()))
end)

-- Update simple radio config serverside.
net.Receive("update_vendemiaire_simple_radio_settings", function(len, ply)
    local frq = net.ReadInt(32)
    local button = net.ReadInt(32)
    local status = net.ReadBool()
    
    local pradio = ply:GetWeapon("vendemiaire_simple_radio")
    if pradio:IsValid() then
        pradio.frequency = frq
        pradio.speak_button = button
	pradio.isactive = status
    end
end)

-- Update double radio config serverside.
net.Receive("update_vendemiaire_double_radio_settings", function(len, ply)
    local radio1_frq = net.ReadInt(32)
    local radio1_button = net.ReadInt(32)
    local radio1_status = net.ReadBool()
    local radio2_frq = net.ReadInt(32)
    local radio2_button = net.ReadInt(32)
    local radio2_status = net.ReadBool()
    
    local pradio = ply:GetWeapon("vendemiaire_double_radio")
    if pradio:IsValid() then
        pradio.radio1_frequency = radio1_frq
        pradio.radio1_speak_button = radio1_button
	pradio.radio1_isactive = radio1_status
	pradio.radio2_frequency = radio2_frq
        pradio.radio2_speak_button = radio2_button
	pradio.radio2_isactive = radio2_status
    end
end)
