util.AddNetworkString("vmradio_send_message_simple_radio")
util.AddNetworkString("vmradio_send_message_double_radio_1")
util.AddNetworkString("vmradio_send_message_double_radio_2")
util.AddNetworkString("vmradio_change_is_receiving_state")

--[[
   A function that return if the player is on the given frequency.

   ply, player we want to know if he is on the given frequency.
   frequency, the frequency on which the player should to be.
--]]
local function ply_on_frequency(ply, frequency)
   local ply_simple_radio = ply:GetWeapon("vendemiaire_simple_radio")
   if ply_simple_radio:IsValid() then
      if ply_simple_radio.isactive and ply_simple_radio.frequency == frequency then
	 return true
      end
   end

   local ply_double_radio = ply:GetWeapon("vendemiaire_double_radio")
   if ply_double_radio:IsValid() then
      if (
	 ply_double_radio.radio1_isactive and ply_double_radio.radio1_frequency == frequency
	 or (ply_double_radio.radio2_isactive and ply_double_radio.radio2_frequency == frequency)
      )then
	 return true
      end
   end
   return false
end


--[[
   print the given message on the player's chat.
   
   ply, player to send the message to.
   sender, player that send the message.
   frequency, the frequency at which the message is sent.
   text, the content of the message.
--]]
local function print_message_on_player_radio(ply, sender, frequency, text)
   if text == nil then return end
   
   if GetConVar("vmradio_frequency_radio_show_name"):GetBool() then
      ply:ChatPrint("(Radio ".. frequency.. ") ".. sender:Nick().. " : ".. text)
   else
      ply:ChatPrint("(Radio ".. frequency.. ") : ".. text)
   end
end

--[[
   Try to change the player radio is_receiving state   
   ply, the player that own the radio that should shange recieving state.
   frequency, the frequency where the state change.
   state, the radio state that should be the new one.
--]]
function vendemiaire_radio_change_radio_is_receiving_state(ply, frequency, state)
   local simple_radio = ply:GetWeapon("vendemiaire_simple_radio")
   if simple_radio:IsValid() then
      if not(simple_radio.is_receiving == state) then
	 if simple_radio.frequency == frequency then
	    simple_radio.is_receiving = state
	    -- Send net message to client.
	    net.Start("vmradio_change_is_receiving_state")
	    net.WriteString("sr")
	    net.WriteBool(state)
	    net.Send(ply)
	 end
      end
   end

   local double_radio = ply:GetWeapon("vendemiaire_double_radio")
   if not double_radio:IsValid() then return end
   if not(double_radio.radio1_is_receiving == state) then
      if double_radio.radio1_frequency == frequency then
	 double_radio.radio1_is_receiving = state
	 -- Send net message to client.
	 net.Start("vmradio_change_is_receiving_state")
	 net.WriteString("dr1")
	 net.WriteBool(state)
	 net.Send(ply)
      end
   end
   
   if not(double_radio.radio2_is_receiving == state) then
      if double_radio.radio2_frequency == frequency then
	 double_radio.radio2_is_receiving = state
	 -- Send net message to client.
	 net.Start("vmradio_change_is_receiving_state")
	 net.WriteString("dr2")
	 net.WriteBool(state)
	 net.Send(ply)
      end
   end
end

--[[
   When a player could hear a voice, check if the talker is speaking in his radio.
   If he is and the listener is on the same frequence, let the listener hear the voice.
--]]
hook.Add("PlayerCanHearPlayersVoice", "vendemiaire_radio_player_can_hear_voice",function(listener, talker)
    if listener == talker then return end

    local t_simple_radio = talker:GetWeapon("vendemiaire_simple_radio") -- talker's simple radio.
    if t_simple_radio:IsValid() then
       if (
	  t_simple_radio.isactive
	  and t_simple_radio.speak_button_status
	  and ply_on_frequency(listener, t_simple_radio.frequency)
       )then
	  vendemiaire_radio_change_radio_is_receiving_state(listener, t_simple_radio.frequency, true)
	  return true
       end
    end

    local t_double_radio = talker:GetWeapon("vendemiaire_double_radio") -- talker's double radio.
    if t_double_radio:IsValid() then
       if (
	  t_double_radio.radio1_isactive
	  and t_double_radio.radio1_speak_button_status
	  and ply_on_frequency(listener, t_double_radio.radio1_frequency)
       )then
	  vendemiaire_radio_change_radio_is_receiving_state(listener, t_double_radio.radio1_frequency, true)
	  return true
       end
	  
       if (
	  t_double_radio.radio2_isactive
	  and t_double_radio.radio2_speak_button_status
	  and ply_on_frequency(listener, t_double_radio.radio2_frequency)
       )then
	  vendemiaire_radio_change_radio_is_receiving_state(listener, t_double_radio.radio2_frequency, true)
	  return true
       end
    end
end)


--[[
   Chat :
   When a player use a radio command he send a net message.
   Here will be the receives for these net messages.
   We check if the sender have the commande's linked radio active and send his text to all player on the same frequency. 
--]]

net.Receive("vmradio_send_message_simple_radio", function(len, ply)
	       local message = net.ReadString()
	       
	       local w_simple_radio = ply:GetWeapon("vendemiaire_simple_radio") -- Writer simple radio.
	       if w_simple_radio:IsValid() then
		  if w_simple_radio.isactive then
		     ply:EmitSound("radio/on_message.wav")
		     for i, reader in ipairs(player.GetAll()) do
			if ply_on_frequency(reader, w_simple_radio.frequency) then
			   print_message_on_player_radio(reader, ply, w_simple_radio.frequency, message)
			   reader:EmitSound("radio/on_message.wav")
			end
		     end
		  end
	       end
end)

net.Receive("vmradio_send_message_double_radio_1", function(len, ply)
	       local message = net.ReadString()

	       local w_double_radio = ply:GetWeapon("vendemiaire_double_radio") -- Writer double radio.
	       if w_double_radio:IsValid() then
		  if w_double_radio.radio1_isactive then
		     ply:EmitSound("radio/on_message.wav")
		     for i, reader in ipairs(player.GetAll()) do
			if ply_on_frequency(reader, w_double_radio.radio1_frequency) then
			   print_message_on_player_radio(reader, ply, w_double_radio.radio1_frequency, message)
			   reader:EmitSound("radio/on_message.wav")
			end
		     end
		  end
	       end
end)

net.Receive("vmradio_send_message_double_radio_2", function(len, ply)
	       local message = net.ReadString()

	       local w_double_radio = ply:GetWeapon("vendemiaire_double_radio") -- Writer double radio.
	       if w_double_radio:IsValid() then
		  if w_double_radio.radio2_isactive then
		     ply:EmitSound("radio/on_message.wav")
		     for i, reader in ipairs(player.GetAll()) do
			if ply_on_frequency(reader, w_double_radio.radio2_frequency) then
			   print_message_on_player_radio(reader, ply, w_double_radio.radio2_frequency, message)
			   reader:EmitSound("radio/on_message.wav")
			end
		     end
		  end
	       end
end)
