--[[
   When the player press an  active radio's speak button we set button's status to true,
   we enable the player microphone (like when he press the +voicerecord bind) and we emit a sound.  
--]]
hook.Add("PlayerButtonDown", "vendemiaire_radio_speak_button_status_down", function(ply, button)
    local simple_radio = ply:GetWeapon("vendemiaire_simple_radio")
    if simple_radio:IsValid() then
        if simple_radio.isactive then
	   if button == simple_radio.speak_button then
	      simple_radio.speak_button_status = true
	      if CLIENT then
		 permissions.EnableVoiceChat(true)
		 if IsFirstTimePredicted() then
		    simple_radio.is_speaking = true
		 end
	      end
	   end
	end
    end

    local double_radio = ply:GetWeapon("vendemiaire_double_radio")
    if double_radio:IsValid() then
       if double_radio.radio1_isactive and button == double_radio.radio1_speak_button then
	  double_radio.radio1_speak_button_status = true
	  if CLIENT then
	     permissions.EnableVoiceChat(true)
	     if IsFirstTimePredicted() then
		double_radio.radio1_is_speaking = true
	     end
	  end
       end
       
       if double_radio.radio2_isactive and button == double_radio.radio2_speak_button then
	  double_radio.radio2_speak_button_status = true
	  if CLIENT then
	     permissions.EnableVoiceChat(true)
	     if IsFirstTimePredicted() then
		double_radio.radio2_is_speaking = true
	     end
	  end
       end
    end
end)

--[[
   When the player release an active radio's speak button we set button's status to false and
   we disable the player microphone (like when he release the +voicerecord bind).  
--]]
hook.Add("PlayerButtonUp", "vendemiaire_radio_speak_button_status_up", function(ply, button)
    local simple_radio = ply:GetWeapon("vendemiaire_simple_radio")
    if simple_radio:IsValid() then
        if button == simple_radio.speak_button then
            if CLIENT then
	       permissions.EnableVoiceChat(false)
	       simple_radio.is_speaking = false
            end
	    -- Send to all clients on the frequency that someone stop speaking to change receiving icon.
	    if SERVER then
	       for k, player in pairs(player.GetAll()) do
		  if player != ply then
		     vendemiaire_radio_change_radio_is_receiving_state(player, simple_radio.frequency, false)
		  end
	       end
	    end
            simple_radio.speak_button_status = false
        end
    end

    local double_radio = ply:GetWeapon("vendemiaire_double_radio")
    if double_radio:IsValid() then
       if button == double_radio.radio1_speak_button then
	  if CLIENT then
	     permissions.EnableVoiceChat(false)
	     double_radio.radio1_is_speaking = false
	  end
	  -- Send to all clients on the frequency that someone stop speaking to change receiving icon.
	  if SERVER then
	     for k, player in pairs(player.GetAll()) do
		if player != ply then
		   vendemiaire_radio_change_radio_is_receiving_state(player, double_radio.radio1_frequency, false)
		end
	     end
	  end
	  double_radio.radio1_speak_button_status = false
       end
    end
	  
    if button == double_radio.radio2_speak_button then
       if CLIENT then
	  permissions.EnableVoiceChat(false)
	  double_radio.radio2_is_speaking = false
       end
       -- Send to all clients on the frequency that someone stop speaking to change receiving icon.
       if SERVER then
	  for k, player in pairs(player.GetAll()) do
	     if player != ply then
		vendemiaire_radio_change_radio_is_receiving_state(player, double_radio.radio2_frequency, false)
	     end
	  end
       end
       double_radio.radio2_speak_button_status = false
        if button == double_radio.radio1_speak_button then
            if CLIENT then
                permissions.EnableVoiceChat(false)
            end
            double_radio.radio1_speak_button_status = false
        end

	if button == double_radio.radio2_speak_button then
	   if CLIENT then
                permissions.EnableVoiceChat(false)
            end
            double_radio.radio2_speak_button_status = false
        end
    end
end)
