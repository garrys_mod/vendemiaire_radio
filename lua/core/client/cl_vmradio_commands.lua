-- Concommand to send a text message on the simple radio.
concommand.Add("vmradio_send_message_simple_radio", function(ply, cmd, args, argstr)
    if argstr == nil then return end
		  
    net.Start("vmradio_send_message_simple_radio")
        net.WriteString(argstr)
        net.SendToServer()
end)

-- Chat command to send a text message on the simple radio.
hook.Add("OnPlayerChat", "vmradio_send_message_simple_radio", function(ply, text, team_chat, is_dead)
    if ply == LocalPlayer() then
       -- Ckeck if the message start with the command and get the arguments.
       if string.StartWith(text, "/simple_radio") then
	  argstr = string.Split(text, "/simple_radio ")[2]
       elseif string.StartWith(text, "/sr") then
	  argstr = string.Split(text, "/sr ")[2]
       else
	  return
       end
       
       -- Return if there is no target.
       if argstr == nil then return end
       
       -- Send the net message to server.
       net.Start("vmradio_send_message_simple_radio")
       net.WriteString(argstr)
       net.SendToServer()
    end
end)

-- Concommand to send a text message on the double radio 1.
concommand.Add("vmradio_send_message_double_radio_1", function(ply, cmd, args, argstr)
    if argstr == nil then return end
		  
    net.Start("vmradio_send_message_double_radio_1")
        net.WriteString(argstr)
        net.SendToServer()
end)

-- Chat command to send a text message on the double radio 1.
hook.Add("OnPlayerChat", "vmradio_send_message_double_radio_1", function(ply, text, team_chat, is_dead)
    if ply == LocalPlayer() then
       -- Ckeck if the message start with the command and get the arguments.
       if string.StartWith(text, "/double_radio1") then
	  argstr = string.Split(text, "/double_radio1 ")[2]
       elseif string.StartWith(text, "/dr1") then
	  argstr = string.Split(text, "/dr1 ")[2]
       else
	  return
       end
       
       -- Return if there is no target.
       if argstr == nil then return end
       
       -- Send the net message to server.
       net.Start("vmradio_send_message_double_radio_1")
       net.WriteString(argstr)
       net.SendToServer()
    end
end)

-- Concommand to send a text message on the double radio 2.
concommand.Add("vmradio_send_message_double_radio_2", function(ply, cmd, args, argstr)
    if argstr == nil then return end
		  
    net.Start("vmradio_send_message_double_radio_2")
        net.WriteString(argstr)
        net.SendToServer()
end)

-- Chat command to send a text message on the double radio 2.
hook.Add("OnPlayerChat", "vmradio_send_message_double_radio_2", function(ply, text, team_chat, is_dead)
    if ply == LocalPlayer() then
       -- Ckeck if the message start with the command and get the arguments.
       if string.StartWith(text, "/double_radio2") then
	  argstr = string.Split(text, "/double_radio2 ")[2]
       elseif string.StartWith(text, "/dr2") then
	  argstr = string.Split(text, "/dr2 ")[2]
       else
	  return
       end
       
       -- Return if there is no target.
       if argstr == nil then return end
       
       -- Send the net message to server.
       net.Start("vmradio_send_message_double_radio_2")
       net.WriteString(argstr)
       net.SendToServer()
    end
end)

-- Concommand to send a text message on the double radio both radio.
concommand.Add("vmradio_send_message_double_radio", function(ply, cmd, args, argstr)
    if argstr == nil then return end
		  
    net.Start("vmradio_send_message_double_radio_1")
    net.WriteString(argstr)
    net.SendToServer()
    
    net.Start("vmradio_send_message_double_radio_2")
    net.WriteString(argstr)
    net.SendToServer()
end)

-- Chat command to send a text message on the double radio both radio.
hook.Add("OnPlayerChat", "vmradio_send_message_double_radio", function(ply, text, team_chat, is_dead)
    if ply == LocalPlayer() then
       -- Ckeck if the message start with the command and get the arguments.
       if string.StartWith(text, "/double_radio") then
	  argstr = string.Split(text, "/double_radio ")[2]
       elseif string.StartWith(text, "/dr") then
	  argstr = string.Split(text, "/dr ")[2]
       else
	  return
       end
       
       -- Return if there is no target.
       if argstr == nil then return end
       
       -- Send the net messages to server.
       net.Start("vmradio_send_message_double_radio_1")
       net.WriteString(argstr)
       net.SendToServer()

       net.Start("vmradio_send_message_double_radio_2")
       net.WriteString(argstr)
       net.SendToServer()
    end
end)
