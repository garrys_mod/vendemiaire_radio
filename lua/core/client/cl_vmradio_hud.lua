-- Make the HUD when a radio is active
local function hud_vendemiaire_radio()
   -- Simple radio HUD.
   local p_simple_radio = LocalPlayer():GetWeapon("vendemiaire_simple_radio")   
   if p_simple_radio:IsValid() and p_simple_radio.isactive then
      -- Background.
      draw.RoundedBox(10, 22, 230, 200, 35, Color(0, 0, 0, 100))

      -- Frequency and speak button.
      draw.SimpleText("Frequency : ".. p_simple_radio.frequency, "GModNotify", 100, 230, Color(150, 0, 0, 200), TEXT_ALIGN_CENTER)
      draw.SimpleText("(".. input.GetKeyName(p_simple_radio.speak_button).. ")", "DermaDefaultBold", 100, 250, Color(150, 0, 0, 200), TEXT_ALIGN_CENTER)

      -- Microphone icon.
      if p_simple_radio.is_speaking then
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_white_mic.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(170, 235, 20, 20)
      else
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_gray_mic.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(170, 235, 20, 20)
      end

      -- Speaker icon.
      if p_simple_radio.is_receiving then
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_white_speaker.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(192, 235, 20, 20)
      else
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_gray_speaker.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(192, 235, 20, 20)
      end
   end

   -- Double radio 1 HUD.
   local p_double_radio = LocalPlayer():GetWeapon("vendemiaire_double_radio")
   if not p_double_radio:IsValid() then return end
   if p_double_radio.radio1_isactive then
      -- Background.
      draw.RoundedBox(10, 22, 280, 200, 35, Color(0, 0, 0, 100))
      
      -- Frequency and speak button double radio 1.
      draw.SimpleText("Frequency : ".. p_double_radio.radio1_frequency, "GModNotify", 100, 280, Color(0, 150, 0, 200), TEXT_ALIGN_CENTER)
      draw.SimpleText("(".. input.GetKeyName(p_double_radio.radio1_speak_button).. ")", "DermaDefaultBold", 100, 300, Color(0, 150, 0, 200), TEXT_ALIGN_CENTER)

      -- Microphone icon double radio 1.
      if p_double_radio.radio1_is_speaking then
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_white_mic.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(170, 285, 20, 20)
      else
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_gray_mic.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(170, 285, 20, 20)
      end

      -- Speaker icon double radio 1.
      if p_double_radio.radio1_is_receiving then
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_white_speaker.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(192, 285, 20, 20)
      else
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_gray_speaker.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(192, 285, 20, 20)
      end
   end

   -- Double radio 2 HUD
   if p_double_radio.radio2_isactive then
      -- Background.
      draw.RoundedBox(10, 22, 330, 200, 35, Color(0, 0, 0, 100))
      
      -- Frequency and speak button double radio 2.
      draw.SimpleText("Frequency : ".. p_double_radio.radio2_frequency, "GModNotify", 100, 330, Color(0, 0, 150, 200), TEXT_ALIGN_CENTER)
      draw.SimpleText("(".. input.GetKeyName(p_double_radio.radio2_speak_button).. ")", "DermaDefaultBold", 100, 350, Color(0, 0, 150, 200), TEXT_ALIGN_CENTER)
   
      -- Microphone icon double radio 2.
      if p_double_radio.radio2_is_speaking then
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_white_mic.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(170, 335, 20, 20)
      else
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_gray_mic.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(170, 335, 20, 20)
      end

      -- Speaker icon double radio 2.
      if p_double_radio.radio2_is_receiving then
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_white_speaker.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(192, 335, 20, 20)
      else
	 surface.SetMaterial(Material("materials/vmradio_icons/vmradio_gray_speaker.png"))
	 surface.SetDrawColor(255,255,255)
	 surface.DrawTexturedRect(192, 335, 20, 20)
      end
   end
end

hook.Add("HUDPaint", "load_hud_vendemiaire_radio", hud_vendemiaire_radio) -- Load HUD.
