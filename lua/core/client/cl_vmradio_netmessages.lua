-- Load the setting's convars and send it to server.
net.Receive("load_vendemiaire_simple_radio_client_settings", function(len, ply)
    local radio = LocalPlayer():GetWeapon("vendemiaire_simple_radio")
    radio.frequency = GetConVar("simple_radio_frequency"):GetInt()
    radio.speak_button = GetConVar("simple_radio_speak_button"):GetInt()
    radio.isactive = GetConVar("simple_radio_isactive"):GetBool()

    -- Send the config update to the server.
    net.Start("update_vendemiaire_simple_radio_settings")
        net.WriteInt(radio.frequency, 32)
	net.WriteInt(radio.speak_button, 32)
	net.WriteBool(radio.isactive)
    net.SendToServer()
end)

-- Load the setting's convars and send it to server.
net.Receive("load_vendemiaire_double_radio_client_settings", function(len, ply)
    local radio = LocalPlayer():GetWeapon("vendemiaire_double_radio")
    radio.radio1_frequency = GetConVar("double_radio1_frequency"):GetInt()
    radio.radio1_speak_button = GetConVar("double_radio1_speak_button"):GetInt()
    radio.radio1_isactive = GetConVar("double_radio1_isactive"):GetBool()
    radio.radio2_frequency = GetConVar("double_radio2_frequency"):GetInt()
    radio.radio2_speak_button = GetConVar("double_radio2_speak_button"):GetInt()
    radio.radio2_isactive = GetConVar("double_radio2_isactive"):GetBool()
    
    -- Send the config update to the server.
    net.Start("update_vendemiaire_double_radio_settings")
        net.WriteInt(radio.radio1_frequency, 32)
	net.WriteInt(radio.radio1_speak_button, 32)
	net.WriteBool(radio.radio1_isactive)
	net.WriteInt(radio.radio2_frequency, 32)
	net.WriteInt(radio.radio2_speak_button, 32)
	net.WriteBool(radio.radio2_isactive)
    net.SendToServer()
end)

-- Change the radio is_receiving state.
net.Receive("vmradio_change_is_receiving_state", function(len, ply)
	       radio_type = net.ReadString()
	       state = net.ReadBool()

	       if radio_type == "sr" then
		  local radio = LocalPlayer():GetWeapon("vendemiaire_simple_radio")
		  if not(radio:IsValid()) then return end

		  radio.is_receiving = state
	       else
		  local radio = LocalPlayer():GetWeapon("vendemiaire_double_radio")
		  if not radio:IsValid() then return end
		  if radio_type == "dr1" then
		     radio.radio1_is_receiving = state
		  end
		  if radio_type == "dr2" then
		     radio.radio2_is_receiving = state
		  end
	       end
end)
