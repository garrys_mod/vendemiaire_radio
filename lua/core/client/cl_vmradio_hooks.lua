-- Start a sound when the client speak on radio (press radio speak button).
hook.Add("PlayerStartVoice", "vendemiaire_radio_sound_on_speak", function(ply)
   if ply:GetWeapon("vendemiaire_simple_radio"):IsValid() then
      if input.IsButtonDown(ply:GetWeapon("vendemiaire_simple_radio").speak_button) then
	 ply:EmitSound("radio/speak_on_radio.wav")
      end
   end

   if ply:GetWeapon("vendemiaire_double_radio"):IsValid() then
      if(
	 input.IsButtonDown(ply:GetWeapon("vendemiaire_double_radio").radio1_speak_button)
	 or input.IsButtonDown(ply:GetWeapon("vendemiaire_double_radio").radio2_speak_button)
      )then
	 ply:EmitSound("radio/speak_on_radio.wav")
      end
   end
end)

-- When player start speaking, check if he speak on a radio and enable microphone icon.
hook.Add("PlayerStartVoice", "vendemiaire_radio_enable_mic_icon_on_voice_start", function(ply)
	    if ply != LocalPlayer() then return end
	    local simple_radio = LocalPlayer():GetWeapon("vendemiaire_simple_radio")
	    if simple_radio:IsValid() and simple_radio.isactive and simple_radio.speak_button_status then
	       simple_radio.is_speaking = true
	    end
	    
	    local double_radio = LocalPlayer():GetWeapon("vendemiaire_double_radio")
	    if double_radio:IsValid() and double_radio.radio1_isactive and double_radio.radio1_speak_button_status then
	       double_radio.radio1_is_speaking = true
	    end
	    
	    if double_radio:IsValid() and double_radio.radio2_isactive and double_radio.radio2_speak_button_status then
	       double_radio.radio2_is_speaking = true
	    end
end)

hook.Add("PlayerEndVoice", "vendemiaire_radio_disable_mic_icon_on_voice_end", function(ply)
	    if ply != LocalPlayer() then return end
	    local simple_radio = LocalPlayer():GetWeapon("vendemiaire_simple_radio")
	    if simple_radio:IsValid() and simple_radio.is_speaking then
	       simple_radio.is_speaking = false
	    end
	    
	    local double_radio = LocalPlayer():GetWeapon("vendemiaire_double_radio")
	    if double_radio:IsValid() and double_radio.radio1_is_speaking then
	       double_radio.radio1_is_speaking = false
	    end
	    
	    if double_radio:IsValid() and double_radio.radio2_is_speaking then
	       double_radio.radio2_is_speaking = false
	    end
end)
