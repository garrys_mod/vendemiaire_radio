CreateConVar("simple_radio_speak_button", 24, FCVAR_ARCHIVE)
CreateConVar("simple_radio_frequency", 1, FCVAR_ARCHIVE)
CreateConVar("simple_radio_isactive", 1, FCVAR_ARCHIVE)

CreateConVar("double_radio1_speak_button", 12, FCVAR_ARCHIVE)
CreateConVar("double_radio1_frequency", 1, FCVAR_ARCHIVE)
CreateConVar("double_radio1_isactive", 1, FCVAR_ARCHIVE)

CreateConVar("double_radio2_speak_button", 13, FCVAR_ARCHIVE)
CreateConVar("double_radio2_frequency", 2, FCVAR_ARCHIVE)
CreateConVar("double_radio2_isactive", 1, FCVAR_ARCHIVE)

-- Command to change the show name convar.
concommand.Add("switch_frequency_radio_show_name", function(ply, cmd, args)
    net.Start("switch_frequency_radio_show_name")
    net.SendToServer()
end)
