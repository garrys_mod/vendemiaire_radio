print("------------------")
print("Vendemiaire radio")

include("core/server/sv_vmradio_convars.lua")
include("core/server/sv_vmradio_netmessages.lua")
include("core/server/sv_vmradio_hooks.lua")

include("core/shared/sh_vmradio_hooks.lua")

AddCSLuaFile("core/client/cl_vmradio_convars.lua")
AddCSLuaFile("core/client/cl_vmradio_netmessages.lua")
AddCSLuaFile("core/client/cl_vmradio_hooks.lua")
AddCSLuaFile("core/client/cl_vmradio_hud.lua")
AddCSLuaFile("core/client/cl_vmradio_commands.lua")

AddCSLuaFile("core/shared/sh_vmradio_hooks.lua")

print("------------------")
