print("------------------")
print("Vendemiaire radio")

include("core/client/cl_vmradio_convars.lua")
include("core/client/cl_vmradio_netmessages.lua")
include("core/client/cl_vmradio_hooks.lua")
include("core/client/cl_vmradio_hud.lua")
include("core/client/cl_vmradio_commands.lua")

include("core/shared/sh_vmradio_hooks.lua")

print("------------------")
